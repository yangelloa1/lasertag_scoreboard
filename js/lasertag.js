//Needed for AWS and MQTT functionality
var awsIot = require('aws-iot-device-sdk');
//Needed for file altering functionality
var fs = require('fs');

//The server
var device;
//Team scores and member IDs
var team1Ids, team1Score = 0;
var team2Ids, team2Score = 0;
//Whether the game is started or not
//1 started, 0 not started
var start = 0;

/**
 * Initializes the connection between aws and this device
 */
exports.connect = function(){

    //Initialize device information (i.e. security keys, certificates, region)
    device = awsIot.device({
        keyPath: '72f9359890-private.pem.key',
        certPath: '72f9359890-certificate.pem.crt',
        caPath: 'rootCA.pem',
        clientId: 'Server',
        region: 'us-east-1',
        host: 'a213qaaw8pshxz.iot.us-east-1.amazonaws.com'
    });

    //Actions to perform when connection is established
    device
        .on('connect', function(){
            console.log('Connected');
            //Testing topic
            device.subscribe('ServerPolicy');
            //Game starting topic
            device.subscribe('$aws/things/server/lasertag/start');
            //Game ending topic
            device.subscribe('$aws/things/server/lasertag/end');
            //Player hit topic
            device.subscribe('$aws/things/+/lasertag/hit');
        });

    //Actions to perform when a message is received from a subscibed topic
    device
        .on('message', function(topic, payload){
            //Print the message contents to the console
            console.log('message', topic, payload.toString());
            //Check which topic the message came from
            if(topic == "$aws/things/server/lasertag/start") {start(payload);}
            else if(topic == "$aws/things/server/lasertag/end") {end(payload);}
            else if(/hit/.test(topic)){hit(payload);}
    });
}

/**
 * Actions to perform when a message is received from the start topic
 * @param {String} startData - The payload from the MQTT message
 */
var start = function(startData) {
    //Parse JSON data
    var data = JSON.parse(startData);
    //Collect player IDs from each team
    team1Ids = Object.values(data["team1"]);
    team2Ids = Object.values(data["team2"]);   

    console.log('Teams Set');

    //Reset Score and begin and signal start of game
    //(start = 1 will start the clock)
    team1Score = team2Score = 0; start = 1;
    updateScoreFile();
}

/**
 * Actions to perform when a message is received from the end topic
 * @param {String} endData - The payload from the MQTT message 
 */
var end = function(endData) {
    //Parse JSON data
    var data = JSON.parse(endData);
    //Retieve the winning team name
    var winner = data["winner"];
    //End the game (start = 0 will stop the clock)
    start = 0;
    updateScoreFile();

    console.log('Game Over');
}

/**
 * Actions to perform when a message is received from the hit topic
 * @param {String} hitData - The payload from the MQTT message
 */
var hit = function(hitData) {
    //Parse JSON data
    var data = JSON.parse(hitData);
    //Retrieve the shoort ID
    var id = data["id"];
    //Check to see if the shooter and the hit player
    //are on separate teams and increment score accordingly
    if(team1Ids.includes(id)) {team1Score++;}
    else if(team2Ids.includes(id)) {team2Score++;}

    updateScoreFile();
}

/**
 * Print given data to the given file
 * @param {String} file - The file to which the data will be writen 
 * @param {String} data - The data which will be writen to a file
 */
var printFile = function(file, data) {
    fs.writeFile(file, data, function(err) {
        if(err) {
            return console.log(err);
        }
    });
}

//Update the score.json file with the new score and start values
var updateScoreFile = function() {
    printFile("../score.json", '{"start": ' + start + ',\n"team1-score": ' + team1Score + ',\n"team2-score": ' + team2Score + '}');
}