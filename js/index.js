/**
 * This scoreboard depends on fetching data from a locally stored JSON file
 * and therefore works best when run from a browser operating without caching
 * 
 * This scoreboard updates it's time and scores via polling. Data is retrieved
 * every 1 second.
 * 
 * <img src="scoreboard.png"/>
 * 
 * Design inspired by https://codepen.io/G_4s/pen/gLvrgO
 */

var $detailsContainer = $('#match-details-container');
var $detailsCurtain = $('#match-details-curtain');
var date = new Date();
var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var matchTime = 0;
var team1LogoUrl = 'http://elvis.rowan.edu/~yangelloa1/Cerberus.png';
var team2LogoUrl = 'http://elvis.rowan.edu/~yangelloa1/Dragon.png';

/**
 * fillDetailMatchStatistics populates the scoreboard with the given information
 * @param {String} team1Name - The name of team 1
 * @param {String} team1Logo - The url for the team 1 logo image
 * @param {String, Integer} team1Score - The score of team 1
 * @param {String} team2Name - The name of team 2
 * @param {String} team2Logo - The url for the team 2 logo image
 * @param {String, Integer} team2Score - The score of team 2
 * @param {Date} date - The date of the match
 * @param {String} time - The match time
 */
var fillDetailedMatchStatistics = function(team1Name, team1Logo, team1Score, team2Name, team2Logo, team2Score, date, time) {
    //Populate elements
    $('.team1.logo').css('background-image', 'url("' + team1Logo + '")');
    $('.team1.name').text(team1Name);
    $('.team1.score').text(team1Score);
    $('.team2.logo').css('background-image', 'url("' + team2Logo + '")');
    $('.team2.name').text(team2Name);
    $('.team2.score').text(team2Score);
    $('#date-of-match').text(date.getDate() + ' ' + months[date.getMonth()] + ' ' + date.getFullYear());
    $('#time-of-match').text(time);
    var $team2ScoreEl = $('.team2.score');
    var $team1ScoreEl = $('.team1.score');
    var $team2Score = +$team2ScoreEl.text();
    var $team1Score = +$team1ScoreEl.text();
};

/**
 * Updates the time and score elements 
 */
var updateTimeAndScore = function() {
    //Fetch data from the score JSON file, stored locally
    $.getJSON("./score.json",function(data){
        //Check if the game has started, and update score and match time if so
        if(data["start"] == '1'){
            fixScore(data["team1-score"],$('.team1.score'), data["team2-score"],$('.team2.score'));
            $('#time-of-match').text(formatMatchTime(++matchTime));
        }
    });
}

/**
 * Formats the given duration of the match (in seconds) in a MM:SS format
 * @param {Integer} matchTimeSec - The duration of the match in seconds
 */
var formatMatchTime = function(matchTimeSec){
    var sec = matchTimeSec % 60;
    var min = Math.floor(matchTimeSec / 60);
    if(sec < 10) sec = '0' + sec; 
    if(min < 10) min = '0' + min;
    return min + ':' + sec;
}

/**
 * fixScore sets the teams scores and  controls the color change of the team who is currently winning
 * Specifically, it turn the winning team's score green.
 * @param {String, Integer} team1Score - Team 1's current score
 * @param {HTML Element} team1ScoreEl - The HTML element in which team 1's score is displayed
 * @param {String, Integer} team2Score - Team 2's current score
 * @param {HTML Elment} team2ScoreEl - The HTML element in which team 2's score is displayed
 */
var fixScore = function(team1Score, team1ScoreEl, team2Score, team2ScoreEl) {
    //Set the team scores
    team1ScoreEl.text(team1Score);
    team2ScoreEl.text(team2Score);

    //Check to see who is winning, and change color accordingly
    if(team2Score == team1Score) {
        $(team2ScoreEl, team1ScoreEl).addClass('winner');
    } else if(team2Score > team1Score) {
        team2ScoreEl.addClass('winner');
        team1ScoreEl.removeClass('winner');
    } else {
        team2ScoreEl.removeClass('winner');
        team1ScoreEl.addClass('winner');
    }
}

//Populate the scoreboard with the desired starting statistics
fillDetailedMatchStatistics('Team Cerberus', team1LogoUrl, 0, 'Team Dragon', team2LogoUrl, 0, date, '00:00');

//Update time and score every 1 second
var myVar = setInterval(updateTimeAndScore, 1000);